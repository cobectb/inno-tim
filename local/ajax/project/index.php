<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application, 
    Bitrix\Main\Context, 
    Bitrix\Main\Request, 
    Bitrix\Main\Server;

$context = Application::getInstance()->getContext();
$request = $context->getRequest();
$action = $request->get("action"); 
$page = $request->get("page") ?? 1;
$project_state = $request->get("project_state"); 
$name = $request->get("name"); 
$limit = 6; 
$count = 0;


$responseData = [
	'success' => false,
	'page' => $page,
	'limit' => $limit,
	'count' => $count,
	'message' => '',
	'data' => [],
];
$dateKeyFormat = 'Y-m-d';

if(\Bitrix\Main\Loader::includeModule('usk.showcase'))
{
	switch ($action) {
		case 'list';
		default:
			$manager = new \Usk\Showcase\Model\ProjectTable();
			$params = [
				'filter' => [
					'ACTIVE' => 1,
				],
				'select' => [
					'ID', 
					'NAME', 
					'DESCRIPTION', 
					'LOGO',
					'SOURCE', 
					'UPDATED', 
					'STATE_NAME' => 'STATE.NAME',
				],
				'limit' => $limit,
				'offset' => $limit * ($page - 1),
			];
			if(!empty($project_state)){
				$params['filter']['STATE_ID'] = $project_state;
			}
			if(!empty($name)){
				$params['filter'][] = [
					'LOGIC' => 'OR',
					[
						'NAME' => '%'.$name.'%',
					],
					[
						'DESCRIPTION' => '%'.$name.'%',
					],
				];
			}
			$rows = $manager->getList($params);
			while ($row = $rows->fetch()) {
				if(empty($row['LOGO'])){
					$row['LOGO'] = '/upload/no-logo.png';
				}
				$row['UPDATED'] = $row['UPDATED']->format('d.m.Y');
				$responseData['data'][] = $row;
				$count++;
			}
			$responseData['count'] = $count;
			$responseData['success'] = true;
			break;
	}
}

echo \Bitrix\Main\Web\Json::encode($responseData);

require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/epilog_after.php");