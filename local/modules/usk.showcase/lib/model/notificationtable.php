<?php
namespace Usk\Showcase\Model;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\ORM\Data\DataManager,
	Bitrix\Main\ORM\Fields\DatetimeField,
	Bitrix\Main\ORM\Fields\IntegerField,
	Bitrix\Main\ORM\Fields\BooleanField,
	Bitrix\Main\ORM\Fields\TextField;

Loc::loadMessages(__FILE__);

/**
 * Class NotificationTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> SUBSCRIPTION_ID int optional
 * <li> COMPANY_ID int optional
 * <li> PROJECT_ID int optional
 * <li> CHANNEL text optional
 * <li> VALUE text optional
 * <li> SENT bool optional
 * <li> ATTEMPT int optional
 * <li> CREATED datetime optional
 * <li> UPDATED datetime optional
 * </ul>
 *
 * @package Usk\Showcase
 **/

class NotificationTable extends DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'usk_showcase_notification';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return [
			new IntegerField(
				'ID',
				[
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('NOTIFICATION_ENTITY_ID_FIELD')
				]
			),
			new IntegerField(
				'SUBSCRIPTION_ID',
				[
					'title' => Loc::getMessage('NOTIFICATION_ENTITY_SUBSCRIPTION_ID_FIELD')
				]
			),
			new IntegerField(
				'COMPANY_ID',
				[
					'title' => Loc::getMessage('NOTIFICATION_ENTITY_COMPANY_ID_FIELD')
				]
			),
			new IntegerField(
				'PROJECT_ID',
				[
					'title' => Loc::getMessage('NOTIFICATION_ENTITY_PROJECT_ID_FIELD')
				]
			),
			new TextField(
				'CHANNEL',
				[
					'title' => Loc::getMessage('NOTIFICATION_ENTITY_CHANNEL_FIELD')
				]
			),
			new TextField(
				'VALUE',
				[
					'title' => Loc::getMessage('NOTIFICATION_ENTITY_VALUE_FIELD')
				]
			),
			new BooleanField(
				'SENT',
				[
					'title' => Loc::getMessage('NOTIFICATION_ENTITY_ACTIVE_FIELD'),
					'values' => [0, 1],
				]
			),
			new IntegerField(
				'ATTEMPT',
				[
					'title' => Loc::getMessage('NOTIFICATION_ENTITY_ATTEMPT_FIELD')
				]
			),	
			new DatetimeField(
				'CREATED',
				[
					'title' => Loc::getMessage('NOTIFICATION_ENTITY_CREATED_FIELD')
				]
			),
			new DatetimeField(
				'UPDATED',
				[
					'title' => Loc::getMessage('NOTIFICATION_ENTITY_UPDATED_FIELD')
				]
			),
			new \Bitrix\Main\Entity\ReferenceField(
                'COMPANY',
                '\Usk\Showcase\Model\CompanyTable',
                ['=this.COMPANY_ID' => 'ref.ID']
            ),
			new \Bitrix\Main\Entity\ReferenceField(
                'PROJECT',
                '\Usk\Showcase\Model\ProjectTable',
                ['=this.PROJECT_ID' => 'ref.ID']
            ),
			new \Bitrix\Main\Entity\ReferenceField(
                'SUBSCRIPTION',
                '\Usk\Showcase\Model\SubscriptionTable',
                ['=this.SUBSCRIPTION_ID' => 'ref.ID']
            ),
		];
	}

}