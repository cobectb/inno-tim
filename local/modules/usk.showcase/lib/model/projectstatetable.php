<?php
namespace Usk\Showcase\Model;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\ORM\Data\DataManager,
	Bitrix\Main\ORM\Fields\DatetimeField,
	Bitrix\Main\ORM\Fields\IntegerField,
	Bitrix\Main\ORM\Fields\BooleanField,
	Bitrix\Main\ORM\Fields\TextField;

Loc::loadMessages(__FILE__);

/**
 * Class ProjectStateTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> SORT int optional
 * <li> ACTIVE bool optional
 * <li> NAME text optional
 * </ul>
 *
 * @package Usk\Showcase
 **/

class ProjectStateTable extends DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'usk_showcase_project_state';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return [
			new IntegerField(
				'ID',
				[
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('USK_MAIN_ENTITY_ID_FIELD')
				]
			),
			new IntegerField(
				'SORT',
				[
					'title' => Loc::getMessage('USK_MAIN_ENTITY_SORT_FIELD')
				]
			),
			new BooleanField(
				'ACTIVE',
				[
					'title' => Loc::getMessage('USK_MAIN_ENTITY_ACTIVE_FIELD'),
					'values' => [0, 1],
				]
			),
			new TextField(
				'NAME',
				[
					'title' => Loc::getMessage('USK_MAIN_ENTITY_NAME_FIELD')
				]
			),
		];
	}

	public static function getEnums($params = []){
		$result = [];
		$manager = new self();
		$rows = $manager->getList($params);
		while ($row = $rows->fetch()) {
			$name = trim($row['NAME']);
			$result[$name] = $row['ID'];
		}
		return $result;
	}

	public static function getVariants($params = []){
		if(empty($params['order'])){
			$params['order'] = ['SORT' => 'ASC'];
		}
		$result = [];
		$manager = new self();
		$rows = $manager->getList($params);
		while ($row = $rows->fetch()) {
			$row['NAME'] = trim($row['NAME']);
			$result[] = $row;
		}
		return $result;
	}

}