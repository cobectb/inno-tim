<?php
namespace Usk\Showcase\Model;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\ORM\Data\DataManager,
	Bitrix\Main\ORM\Fields\DatetimeField,
	Bitrix\Main\ORM\Fields\IntegerField,
	Bitrix\Main\ORM\Fields\BooleanField,
	Bitrix\Main\ORM\Fields\TextField;

Loc::loadMessages(__FILE__);

/**
 * Class ProjectTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ACTIVE bool optional
 * <li> EXTERNAL_ID int optional
 * <li> TEAM_ID int optional
 * <li> STATE_ID int optional
 * <li> ORGANIZATION_ID int optional
 * <li> CERTIFICATION_ID int optional
 * <li> ORGSIZE_ID int optional
 * <li> NAME text optional
 * <li> LOGO text optional
 * <li> DESCRIPTION text optional
 * <li> CASES text optional
 * <li> BENEFITS text optional
 * <li> ACCELERATION text optional
 * <li> CONTACTS text optional
 * <li> ARTIFACTS text optional
 * <li> INN text optional
 * <li> SOURCE text optional
 * <li> CREATED datetime optional
 * <li> UPDATED datetime optional
 * </ul>
 *
 * @package Usk\Showcase
 **/

class ProjectTable extends DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'usk_showcase_project';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return [
			new IntegerField(
				'ID',
				[
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('PROJECT_ENTITY_ID_FIELD')
				]
			),
			new BooleanField(
				'ACTIVE',
				[
					'title' => Loc::getMessage('USK_MAIN_ENTITY_ACTIVE_FIELD'),
					'values' => [0, 1],
				]
			),
			new IntegerField(
				'EXTERNAL_ID',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_EXTERNAL_ID_FIELD')
				]
			),
			new IntegerField(
				'TEAM_ID',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_TEAM_ID_FIELD')
				]
			),
			new IntegerField(
				'STATE_ID',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_STATE_ID_FIELD')
				]
			),
			new IntegerField(
				'ORGANIZATION_ID',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_ORGANIZATION_ID_FIELD')
				]
			),
			new IntegerField(
				'CERTIFICATION_ID',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_CERTIFICATION_ID_FIELD')
				]
			),			
			new IntegerField(
				'ORGSIZE_ID',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_ORGSIZE_ID_FIELD')
				]
			),
			new TextField(
				'NAME',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_NAME_FIELD')
				]
			),
			new TextField(
				'LOGO',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_LOGO_FIELD')
				]
			),
			new TextField(
				'DESCRIPTION',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_DESCRIPTION_FIELD')
				]
			),			
			new TextField(
				'CASES',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_CASES_FIELD')
				]
			),			
			new TextField(
				'BENEFITS',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_BENEFITS_FIELD')
				]
			),			
			new TextField(
				'ACCELERATION',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_ACCELERATION_FIELD')
				]
			),			
			new TextField(
				'CONTACTS',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_CONTACTS_FIELD')
				]
			),				
			new TextField(
				'ARTIFACTS',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_ARTIFACTS_FIELD')
				]
			),			
			new TextField(
				'INN',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_INN_FIELD')
				]
			),
			new TextField(
				'SOURCE',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_SOURCE_FIELD')
				]
			),	
			new DatetimeField(
				'CREATED',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_CREATED_FIELD')
				]
			),
			new DatetimeField(
				'UPDATED',
				[
					'title' => Loc::getMessage('PROJECT_ENTITY_UPDATED_FIELD')
				]
			),
			new \Bitrix\Main\Entity\ReferenceField(
                'TEAM',
                '\Usk\Showcase\Model\TeamTable',
                ['=this.TEAM_ID' => 'ref.ID']
            ),
			new \Bitrix\Main\Entity\ReferenceField(
                'STATE',
                '\Usk\Showcase\Model\ProjectStateTable',
                ['=this.STATE_ID' => 'ref.ID']
            ),
    		new \Bitrix\Main\Entity\ReferenceField(
                'ORGANIZATION',
                '\Usk\Showcase\Model\OrganizationTable',
                ['=this.ORGANIZATION_ID' => 'ref.ID']
            ),
        	new \Bitrix\Main\Entity\ReferenceField(
                'CERTIFICATION',
                '\Usk\Showcase\Model\CertificationTable',
                ['=this.CERTIFICATION_ID' => 'ref.ID']
            ),
		];
	}

	public function deleteByExternalID($externalID) {
		$rows = $this->getByExternalID($externalID);
		if(!empty($rows)){
			foreach ($rows as $row) {
				$this->delete($row['ID']);
			}
		}
	}

	public function getByExternalID($externalID) {
		$result = [];
		$rows = $this->getList([
			'filter' => [
				'EXTERNAL_ID' => $externalID,
			]
		]);
		while ($row = $rows->fetch()) {
			$result[] = $row;
		}
		return $result;
	}
}