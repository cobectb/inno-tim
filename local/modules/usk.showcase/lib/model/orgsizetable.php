<?php
namespace Usk\Showcase\Model;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\ORM\Data\DataManager,
	Bitrix\Main\ORM\Fields\DatetimeField,
	Bitrix\Main\ORM\Fields\IntegerField,
	Bitrix\Main\ORM\Fields\BooleanField,
	Bitrix\Main\ORM\Fields\TextField;

Loc::loadMessages(__FILE__);

/**
 * Class OrgsizeTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> SORT int optional
 * <li> ACTIVE bool optional
 * <li> NAME text optional
 * </ul>
 *
 * @package Usk\Showcase
 **/

class OrgsizeTable extends DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'usk_showcase_orgsize';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return [
			new IntegerField(
				'ID',
				[
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('USK_MAIN_ENTITY_ID_FIELD')
				]
			),
			new IntegerField(
				'SORT',
				[
					'title' => Loc::getMessage('USK_MAIN_ENTITY_SORT_FIELD')
				]
			),
			new BooleanField(
				'ACTIVE',
				[
					'title' => Loc::getMessage('USK_MAIN_ENTITY_ACTIVE_FIELD'),
					'values' => [0, 1],
				]
			),
			new TextField(
				'NAME',
				[
					'title' => Loc::getMessage('USK_MAIN_ENTITY_NAME_FIELD')
				]
			),
		];
	}

}