<?php
namespace Usk\Showcase\Model;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\ORM\Data\DataManager,
	Bitrix\Main\ORM\Fields\DatetimeField,
	Bitrix\Main\ORM\Fields\IntegerField,
	Bitrix\Main\ORM\Fields\BooleanField,
	Bitrix\Main\ORM\Fields\TextField;

Loc::loadMessages(__FILE__);

/**
 * Class RequestTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> EXTERNAL_ID int optional
 * <li> COMPANY_ID int optional
 * <li> ACTIVE bool optional
 * <li> COMPLETE bool optional
 * <li> NAME text optional
 * <li> DESCRIPTION text optional
 * <li> CONTACTS text optional
 * <li> ARTIFACTS text optional
 * <li> CREATED datetime optional
 * <li> UPDATED datetime optional
 * </ul>
 *
 * @package Usk\Showcase
 **/

class RequestTable extends DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'usk_showcase_request';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return [
			new IntegerField(
				'ID',
				[
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('REQUEST_ENTITY_ID_FIELD')
				]
			),
			new IntegerField(
				'EXTERNAL_ID',
				[
					'title' => Loc::getMessage('REQUEST_ENTITY_EXTERNAL_ID_FIELD')
				]
			),
			new IntegerField(
				'COMPANY_ID',
				[
					'title' => Loc::getMessage('REQUEST_ENTITY_COMPANY_ID_FIELD')
				]
			),
			new BooleanField(
				'ACTIVE',
				[
					'title' => Loc::getMessage('REQUEST_ENTITY_ACTIVE_FIELD'),
					'values' => [0, 1],
				]
			),	
			new BooleanField(
				'COMPLETE',
				[
					'title' => Loc::getMessage('REQUEST_ENTITY_COMPLETE_FIELD'),
					'values' => [0, 1],
				]
			),	
			new TextField(
				'NAME',
				[
					'title' => Loc::getMessage('REQUEST_ENTITY_NAME_FIELD')
				]
			),
			new TextField(
				'DESCRIPTION',
				[
					'title' => Loc::getMessage('REQUEST_ENTITY_DESCRIPTION_FIELD')
				]
			),									
			new TextField(
				'CONTACTS',
				[
					'title' => Loc::getMessage('REQUEST_ENTITY_CONTACTS_FIELD')
				]
			),				
			new TextField(
				'ARTIFACTS',
				[
					'title' => Loc::getMessage('REQUEST_ENTITY_ARTIFACTS_FIELD')
				]
			),			
			new DatetimeField(
				'CREATED',
				[
					'title' => Loc::getMessage('REQUEST_ENTITY_CREATED_FIELD')
				]
			),
			new DatetimeField(
				'UPDATED',
				[
					'title' => Loc::getMessage('REQUEST_ENTITY_UPDATED_FIELD')
				]
			),
			new \Bitrix\Main\Entity\ReferenceField(
                'COMPANY',
                '\Usk\Showcase\Model\CompanyTable',
                ['=this.COMPANY_ID' => 'ref.ID']
            ),
		];
	}

	public function deleteByExternalID($externalID) {
		$rows = $this->getByExternalID($externalID);
		if(!empty($rows)){
			foreach ($rows as $row) {
				$this->delete($row['ID']);
			}
		}
	}

	public function getByExternalID($externalID) {
		$result = [];
		$rows = $this->getList([
			'filter' => [
				'EXTERNAL_ID' => $externalID,
			]
		]);
		while ($row = $rows->fetch()) {
			$result[] = $row;
		}
		return $result;
	}
}