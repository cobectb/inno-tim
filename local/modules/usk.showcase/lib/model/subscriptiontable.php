<?php
namespace Usk\Showcase\Model;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\ORM\Data\DataManager,
	Bitrix\Main\ORM\Fields\DatetimeField,
	Bitrix\Main\ORM\Fields\IntegerField,
	Bitrix\Main\ORM\Fields\BooleanField,
	Bitrix\Main\ORM\Fields\TextField;

Loc::loadMessages(__FILE__);

/**
 * Class SubscriptionTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> COMPANY_ID int optional
 * <li> PROJECT_ID int optional
 * <li> ACTIVE bool optional
 * <li> CREATED datetime optional
 * <li> UPDATED datetime optional
 * </ul>
 *
 * @package Usk\Showcase
 **/

class SubscriptionTable extends DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'usk_showcase_subscription';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return [
			new IntegerField(
				'ID',
				[
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('SUBSCRIPTION_ENTITY_ID_FIELD')
				]
			),
			new IntegerField(
				'COMPANY_ID',
				[
					'title' => Loc::getMessage('SUBSCRIPTION_ENTITY_COMPANY_ID_FIELD')
				]
			),
			new IntegerField(
				'PROJECT_ID',
				[
					'title' => Loc::getMessage('SUBSCRIPTION_ENTITY_PROJECT_ID_FIELD')
				]
			),
			new BooleanField(
				'ACTIVE',
				[
					'title' => Loc::getMessage('SUBSCRIPTION_ENTITY_ACTIVE_FIELD'),
					'values' => [0, 1],
				]
			),	
			new DatetimeField(
				'CREATED',
				[
					'title' => Loc::getMessage('SUBSCRIPTION_ENTITY_CREATED_FIELD')
				]
			),
			new DatetimeField(
				'UPDATED',
				[
					'title' => Loc::getMessage('SUBSCRIPTION_ENTITY_UPDATED_FIELD')
				]
			),
			new \Bitrix\Main\Entity\ReferenceField(
                'COMPANY',
                '\Usk\Showcase\Model\CompanyTable',
                ['=this.COMPANY_ID' => 'ref.ID']
            ),
			new \Bitrix\Main\Entity\ReferenceField(
                'PROJECT',
                '\Usk\Showcase\Model\ProjectTable',
                ['=this.PROJECT_ID' => 'ref.ID']
            ),
		];
	}

}