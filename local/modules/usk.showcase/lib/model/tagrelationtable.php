<?php
namespace Usk\Showcase\Model;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\ORM\Data\DataManager,
	Bitrix\Main\ORM\Fields\DatetimeField,
	Bitrix\Main\ORM\Fields\IntegerField,
	Bitrix\Main\ORM\Fields\BooleanField,
	Bitrix\Main\ORM\Fields\TextField;

Loc::loadMessages(__FILE__);

/**
 * Class TagTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> PROJECT_ID int optional
 * <li> TAG_ID int optional
 * </ul>
 *
 * @package Usk\Showcase
 **/

class TagRelationTable extends DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'usk_showcase_tag_relation';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return [
			new IntegerField(
				'ID',
				[
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('USK_MAIN_ENTITY_ID_FIELD')
				]
			),
			new IntegerField(
				'PROJECT_ID',
				[
					'title' => Loc::getMessage('USK_MAIN_PROJECT_ID_FIELD')
				]
			),
			new IntegerField(
				'TAG_ID',
				[
					'title' => Loc::getMessage('USK_MAIN_TAG_ID_FIELD')
				]
			),
			new \Bitrix\Main\Entity\ReferenceField(
                'PROJECT',
                '\Usk\Showcase\Model\ProjectTable',
                ['=this.PROJECT_ID' => 'ref.ID']
            ),
			new \Bitrix\Main\Entity\ReferenceField(
                'TAG',
                '\Usk\Showcase\Model\TagTable',
                ['=this.TAG_ID' => 'ref.ID']
            ),

		];
	}

	public function getByProjectID($projectID) {
		$result = [];
		$rows = $this->getList([
			'filter' => [
				'PROJECT_ID' => $projectID,
			],
			'select' => [
				'TAG_NAME' => 'TAG.NAME',
			],
		]);
		while ($row = $rows->fetch()) {
			$result[] = $row['TAG_NAME'];
		}
		return $result;
	}

}