<?php
namespace Usk\Showcase\Model;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\ORM\Data\DataManager,
	Bitrix\Main\ORM\Fields\DatetimeField,
	Bitrix\Main\ORM\Fields\IntegerField,
	Bitrix\Main\ORM\Fields\BooleanField,
	Bitrix\Main\ORM\Fields\TextField;

Loc::loadMessages(__FILE__);

/**
 * Class CompanyTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> EXTERNAL_ID int optional
 * <li> ACTIVE bool optional
 * <li> NAME text optional
 * <li> CREATED datetime optional
 * <li> UPDATED datetime optional
 * </ul>
 *
 * @package Usk\Showcase
 **/

class CompanyTable extends DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'usk_showcase_company';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return [
			new IntegerField(
				'ID',
				[
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('COMPANY_ENTITY_ID_FIELD')
				]
			),
			new IntegerField(
				'EXTERNAL_ID',
				[
					'title' => Loc::getMessage('COMPANY_ENTITY_EXTERNAL_ID_FIELD')
				]
			),
			new BooleanField(
				'ACTIVE',
				[
					'title' => Loc::getMessage('COMPANY_ENTITY_ACTIVE_FIELD'),
					'values' => [0, 1],
				]
			),
			new TextField(
				'NAME',
				[
					'title' => Loc::getMessage('COMPANY_ENTITY_NAME_FIELD')
				]
			),
			new DatetimeField(
				'CREATED',
				[
					'title' => Loc::getMessage('COMPANY_ENTITY_CREATED_FIELD')
				]
			),
			new DatetimeField(
				'UPDATED',
				[
					'title' => Loc::getMessage('COMPANY_ENTITY_UPDATED_FIELD')
				]
			),
		];
	}

	public function deleteByExternalID($externalID) {
		$rows = $this->getByExternalID($externalID);
		if(!empty($rows)){
			foreach ($rows as $row) {
				$this->delete($row['ID']);
			}
		}
	}

	public function getByExternalID($externalID) {
		$result = [];
		$rows = $this->getList([
			'filter' => [
				'EXTERNAL_ID' => $externalID,
			]
		]);
		while ($row = $rows->fetch()) {
			$result[] = $row;
		}
		return $result;
	}
}