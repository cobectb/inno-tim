<?php
namespace Usk\Showcase\Model;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\ORM\Data\DataManager,
	Bitrix\Main\ORM\Fields\DatetimeField,
	Bitrix\Main\ORM\Fields\IntegerField,
	Bitrix\Main\ORM\Fields\BooleanField,
	Bitrix\Main\ORM\Fields\TextField;

Loc::loadMessages(__FILE__);

/**
 * Class PilotTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> EXTERNAL_ID int optional
 * <li> PROJECT_ID int optional
 * <li> TEAM_ID int optional
 * <li> STATE_ID int optional
 * <li> PHASE_ID int optional
 * <li> ORGANIZATION_ID int optional
 * <li> NAME text optional
 * <li> DESCRIPTION text optional
 * <li> CONTACTS text optional
 * <li> ARTIFACTS text optional
 * <li> CREATED datetime optional
 * <li> UPDATED datetime optional
 * </ul>
 *
 * @package Usk\Showcase
 **/

class PilotTable extends DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'usk_showcase_pilot';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return [
			new IntegerField(
				'ID',
				[
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('PILOT_ENTITY_ID_FIELD')
				]
			),
			new IntegerField(
				'EXTERNAL_ID',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_EXTERNAL_ID_FIELD')
				]
			),
			new IntegerField(
				'PROJECT_ID',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_PROJECT_ID_FIELD')
				]
			),
			new IntegerField(
				'TEAM_ID',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_TEAM_ID_FIELD')
				]
			),
			new IntegerField(
				'STATE_ID',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_STATE_ID_FIELD')
				]
			),			
			new IntegerField(
				'PHASE_ID',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_PHASE_ID_FIELD')
				]
			),
			new IntegerField(
				'ORGANIZATION_ID',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_ORGANIZATION_ID_FIELD')
				]
			),		
			new TextField(
				'NAME',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_NAME_FIELD')
				]
			),
			new TextField(
				'DESCRIPTION',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_DESCRIPTION_FIELD')
				]
			),									
			new TextField(
				'CONTACTS',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_CONTACTS_FIELD')
				]
			),				
			new TextField(
				'ARTIFACTS',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_ARTIFACTS_FIELD')
				]
			),			
			new DatetimeField(
				'CREATED',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_CREATED_FIELD')
				]
			),
			new DatetimeField(
				'UPDATED',
				[
					'title' => Loc::getMessage('PILOT_ENTITY_UPDATED_FIELD')
				]
			),
			new \Bitrix\Main\Entity\ReferenceField(
                'PROJECT',
                '\Usk\Showcase\Model\ProjectTable',
                ['=this.PROJECT_ID' => 'ref.ID']
            ),
			new \Bitrix\Main\Entity\ReferenceField(
                'TEAM',
                '\Usk\Showcase\Model\TeamTable',
                ['=this.TEAM_ID' => 'ref.ID']
            ),
			new \Bitrix\Main\Entity\ReferenceField(
                'STATE',
                '\Usk\Showcase\Model\PilotStateTable',
                ['=this.STATE_ID' => 'ref.ID']
            ),
    		new \Bitrix\Main\Entity\ReferenceField(
                'PHASE',
                '\Usk\Showcase\Model\PilotPhaseTable',
                ['=this.PHASE_ID' => 'ref.ID']
            ),
    		new \Bitrix\Main\Entity\ReferenceField(
                'ORGANIZATION',
                '\Usk\Showcase\Model\OrganizationTable',
                ['=this.ORGANIZATION_ID' => 'ref.ID']
            ),
		];
	}

	public function deleteByExternalID($externalID) {
		$rows = $this->getByExternalID($externalID);
		if(!empty($rows)){
			foreach ($rows as $row) {
				$this->delete($row['ID']);
			}
		}
	}

	public function getByExternalID($externalID) {
		$result = [];
		$rows = $this->getList([
			'filter' => [
				'EXTERNAL_ID' => $externalID,
			]
		]);
		while ($row = $rows->fetch()) {
			$result[] = $row;
		}
		return $result;
	}
}