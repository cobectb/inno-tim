<?php
namespace Usk\Showcase\Import;

class Csv extends \Usk\Showcase\Import\Provider
{
	protected $url;

	public function recieve($params = []) {
        $result = [];
        $url = $this->getUrl();
        if(!empty($url)){
            $filePath = $_SERVER['DOCUMENT_ROOT'].$url;
            if(file_exists($filePath)){        
                $membersPath = $_SERVER['DOCUMENT_ROOT'].$this->membersCSV;
                $limit = $params['limit'] = 30;
                $start = $params['start'] ?? 1;
                $stop = $params['stop'] ?? ($start + $limit);
                $index = 0;

                if(($handle = fopen($filePath, "r")) !== false) {
                    $headers = fgetcsv($handle);
                    while (($data = fgetcsv($handle)) !== false) {
                        $index++;
                        if($index >= $start && $index <= $stop){
                            $accident = array_combine($headers, $data);
                            $result[] = $accident;
                        }
                        if($index > $stop){
                            break;
                        }
                    }  
                }
                fclose($handle);

            } else {
                var_dump($filePath);
            }
        }
        return $result;
	}

}