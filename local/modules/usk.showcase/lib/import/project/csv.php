<?php
namespace Usk\Showcase\Import\Project;

class Csv extends \Usk\Showcase\Import\Csv
{
	protected $url;
    protected $pseudo = [
        'Название' => 'NAME',
        'Описание' => 'DESCRIPTION',
        'Ссылка' => 'SOURCE',
        'Статус' => 'STATE_ID',
    ];

    protected $stateEnums;
    protected $scheme;

    function __construct($url = '/upload/data/projects.csv') {
        if($url !== null) {
            $this->url = $url;
        }
        $this->stateEnums = \Usk\Showcase\Model\ProjectStateTable::getEnums();
        $this->manager = new \Usk\Showcase\Model\ProjectTable();
        $this->uniqueKey = 'NAME';
        $map = $this->manager->getMap();
        foreach ($map as $field) {
            $key = $field->getName();
            if($key !== 'ID'){
                $this->scheme[$key] = '';
            }
        }
    }

    public function prepare($data = []) {
        $result = [];
        foreach ($data as $index => $row) {
            $preparedRow = [];
            foreach ($row as $key => $value) {
                if(isset($this->pseudo[$key])){
                    $value = trim($value);
                    $keyValue = $this->pseudo[$key];
                    if($keyValue == 'STATE_ID'){
                        $value = $this->stateEnums[$value];
                    }
                    if($keyValue == 'SOURCE'){
                        $value = self::prepareUrl($value);
                    }
                    $preparedRow[$keyValue] = $value;
                }
            }
            if(!empty($preparedRow)){
                $preparedRow['ACTIVE'] = 1;
                $result[] = array_merge($this->scheme, $preparedRow);
            }
        }
        return $result;
    }


}