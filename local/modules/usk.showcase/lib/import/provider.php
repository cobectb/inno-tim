<?php
namespace Usk\Showcase\Import;

Class Provider {

	protected $url;
	protected $data = [];
	protected $manager;
	protected $uniqueKey;

	function __construct($url = null) {
		if($url !== null) {
			$this->url = $url;
		}
	}

	public function getUrl(){
		return $this->url;
	}

	public function recieve($params = []) {}

	public function prepare($data = []) {
		return $data;
	}

	public static function prepareURL($url = ''){
		$result = trim($url);
		if(!empty($result)){
			if(!preg_match('/^http(s)?/i', $result)){
				$result = sprintf('https://%s', $result);
			}
		}
		return $result;
	}

	public function getData($params = []){
		$data = $this->prepare($this->recieve($params));
		$this->data = $data;
		return $data;
	}

	public function saveData(){
		if(!empty($this->manager) && !empty($this->uniqueKey)){
			foreach ($this->data as $row) {
				$rows = $this->manager->getList(['filter' => [$this->uniqueKey => $row[$this->uniqueKey]]]);
				$row['UPDATED'] = new \Bitrix\Main\Type\DateTime();
				if($exists = $rows->fetch()){
					$existsID = $exists['ID'];
					if($this->uniqueKey === 'ID'){
						unset($row['ID']);
					}
					$result = $this->manager->update($existsID, $row);
				} else {
					$row['CREATED'] = new \Bitrix\Main\Type\DateTime();
					$result = $this->manager->add($row);
				}
				var_dump($result);
			}
		}
	}

}