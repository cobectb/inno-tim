<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

if (class_exists('usk_showcase')) {
    return;
}

class usk_showcase extends CModule
{
    /** @var string */
    public $MODULE_ID;

    /** @var string */
    public $MODULE_VERSION;

    /** @var string */
    public $MODULE_VERSION_DATE;

    /** @var string */
    public $MODULE_NAME;

    /** @var string */
    public $MODULE_DESCRIPTION;

    /** @var string */
    public $MODULE_GROUP_RIGHTS;

    /** @var string */
    public $PARTNER_NAME;

    /** @var string */
    public $PARTNER_URI;

    /** @var array */
    public $models = [
        'Team',
        'ProjectState',
        'Organization',
        'Certification',
        'Orgsize',
        'Project',
        'PilotState',
        'PilotPhase',
        'Pilot',
        'Company',
        'Request',
        'Subscription',
        'Notification',
        'Tag',
    ];

    public function __construct()
    {
        $this->MODULE_ID = 'usk.showcase';
        $this->MODULE_VERSION = '0.1.0';
        $this->MODULE_VERSION_DATE = '2021-12-02 21:22:00';
        $this->MODULE_NAME = Loc::getMessage('USK_SHOWCASE_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('USK_SHOWCASE_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = Loc::getMessage('USK_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('USK_PARTNER_URI');
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->InstallFiles();
        $this->installDB();
        $this->InstallEvents();
    }

    public function DoUninstall()
    {
        $this->UnInstallEvents();
        $this->UnInstallDB();
        $this->UnInstallFiles();
        ModuleManager::unregisterModule($this->MODULE_ID);
    }

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles($arParams = array())
    {
        return true;
    }

    function UnInstallFiles()
    {
        return true;
    }

    function InstallDB($arParams = array())
    {
        global $DB, $APPLICATION;
        if(\Bitrix\Main\Loader::includeModule($this->MODULE_ID)){   
            $connection = \Bitrix\Main\Application::getConnection();
            foreach ($this->models as $modelName) {
                $model = sprintf('\\Usk\\Showcase\\Model\\%sTable', $modelName);
                $tableName = $model::getTableName();
                if(!$connection->isTableExists($tableName)) {
                    $model::getEntity()->createDbTable();
                }
            }
        }
        return true;
    }

    function UnInstallDB($arParams = array())
    {
        global $APPLICATION, $DB, $DOCUMENT_ROOT;
        if(\Bitrix\Main\Loader::includeModule($this->MODULE_ID)){       
            $connection = \Bitrix\Main\Application::getConnection();
            foreach ($this->models as $modelName) {
                $model = sprintf('\\Usk\\Showcase\\Model\\%sTable', $modelName);
                $tableName = $model::getTableName();
                if($connection->isTableExists($tableName)) {
                    $connection->dropTable($tableName);
                }
            }
        }
        return true;
    }
}
