<?
//	\Bitrix\Main\Page\Asset::getInstance()->addCss('/assets/css/bootstrap.min.css');
//	\Bitrix\Main\Page\Asset::getInstance()->addCss('/assets/css/dashboard.css');
	\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/app.css');
	\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH. '/assets/js/app.js', true);
?>
<!DOCTYPE html>
<html lang="en" class="light">
<!-- BEGIN: Head -->
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?$APPLICATION->ShowTitle()?></title>
	<!-- BEGIN: CSS Assets-->
    <?php $APPLICATION->ShowHead();?>
	<!-- END: CSS Assets-->
</head>
<!-- END: Head -->
<body class="main">
	<div class="panel-container">
        <?=$APPLICATION->ShowPanel();?>
	</div>
	<!-- BEGIN: Mobile Menu -->
	<div class="mobile-menu md:hidden">
		<div class="mobile-menu-bar">
			<a href="" class="flex mr-auto">
				<img alt="Inno-TIM" class="w-6" src="<?=SITE_TEMPLATE_PATH?>/assets/images/logo.svg">
			</a>
			<a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
		</div>
	</div>
	<!-- END: Mobile Menu -->
	<div class="flex">
		<!-- BEGIN: Side Menu -->
		<nav class="side-nav">
			<a href="" class="intro-x flex items-center pl-5 pt-4">
				<img alt="Inno-TIM" src="<?=SITE_TEMPLATE_PATH?>/assets/images/white_logo%20(Traced).svg">
			</a>
			<div class="side-nav__devider my-6"></div>
			<ul>

			</ul>
		</nav>
		<!-- END: Side Menu -->
		<!-- BEGIN: Content -->
		<div class="content">
			<!-- BEGIN: Top Bar -->
			<div class="top-bar">
				<!-- BEGIN: Breadcrumb -->
				<div class="-intro-x breadcrumb mr-auto hidden sm:flex"><i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#" class="breadcrumb--active">Каталог решений</a> </div>
				<!-- END: Breadcrumb -->
			</div>
			<!-- END: Top Bar -->