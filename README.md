#### Реализованная функциональность
* Импорт данных из информационных систем через адаптеры (для примера реализована загрузка из CSV-файла);
* Вывод списка проектов подходящих под фильтр.

#### Особенность проекта в следующем:
* Возможность аггрегации из различных источников, через механизм реализации адаптеров;

#### Основной стек технологий:
* Apache + nginx;
* HTML, CSS, JavaScript;
* PHP 7, MySQL;
* Bitrix Framework;
* SASS;
* Webpack, Laravel Mix;
* Git;
* Bitbucket.
  
#### Демо
Демо продукта доступно по адресу: [cobectb.beget.tech](http://cobectb.beget.tech/catalog/)


СРЕДА ЗАПУСКА
------------
1) развертывание продукта производится на любых linux-like серверах или виртуальной машине BitrixVM ([инструкция по настройке](https://dev.1c-bitrix.ru/learning/course/?COURSE_ID=32&CHAPTER_ID=04862&LESSON_PATH=3903.4862));
2) требуется установленный web-сервер с поддержкой PHP(версия 7.4+) интерпретации (apache, nginx);
3) требуется установленная СУБД MariaDB (версия 10+);


УСТАНОВКА
------------
### Установка модуля

Выполните 
~~~
git clone https://cobectb@bitbucket.org/cobectb/inno-tim.git
...
~~~

В административной панели зайдите во вкладку "Решения для Marketplace" -> "Установленные решения".
Нажмите на кнопку-сендвич справа от неустановленного модуля и выберите "Установить".
Импортируйте данные, выполнив в командной PHP-строке скрипт
~~~
if(\Bitrix\Main\Loader::includeModule('usk.showcase')){
	$manager = new \Usk\Showcase\Import\Project\Csv('/upload/data/projects.csv');
	$manager->getData();
	$manager->saveData();
}
~~~

### Для генерации и подключения стилей шаблона необходимо: 

1) ввести команду в терминале: npm i && npm run prod
2) сделать симлинк /layout/dist —> /local/templates/inno-tim/assets

РАЗРАБОТЧИКИ

#### Илья Абросимов — fullstack — https://t.me/abrosimov_online
#### Илья Данилов — fullstack — https://t.me/baklazhanius

#### Павел Роговой — концепция/анализ — https://t.me/wisartelf
#### Ивкин Игорь — концепция/анализ — https://t.me/Sickmilk

### Важно!
Используется бесплатная демо-лицензия системы управления контентом 1C-Битрикс:Управление сайтом в редакции "Старт". Демо решения имеет настройку системы обновления "Установка для разработки".
Типовые элементы HTML-шаблона используют проприетарный код с сайта [themeforest.net](https://themeforest.net/), реквизиты заказа Receipt No: RCD33682799 (Ilya Abrosimov) — повторное использование HTML и CSS элементов в других проектах запрещено.
