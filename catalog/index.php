<?
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog.php"); ?>
<?
    \CJSCore::init(['jquery3']); 
    \Bitrix\Main\Loader::includeModule('usk.showcase');
    $projectStateVariants = \Usk\Showcase\Model\ProjectStateTable::getVariants();
?>
	<style type="text/css">
        #content {
            position: relative;
            min-height: 100px;
            width: 100%;
        }

        .card p {
            margin-bottom: .5rem;
        }

        .loading {
            position: absolute;
            bottom: 1rem;
            left: 0;
            right: 0;
            text-align: center;
            height: 100px;
            z-index: 2;
        }

        .loading img {
            height: 100px;
            margin: 0 auto;
        }
	</style>
	<style type="text/css">
        .form-check {
            display: flex;
            align-items: center
        }

        .form-check-label {
            margin-left: .5rem;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            --tw-text-opacity: 1;
            color: rgba(74,85,104,var(--tw-text-opacity))
        }

        .dark .form-check-label {
            --tw-text-opacity: 1;
            color: rgba(160,174,192,var(--tw-text-opacity))
        }

        .form-check-input:focus {
            outline: 2px solid transparent;
            outline-offset: 2px;
            --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
            --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(3px + var(--tw-ring-offset-width)) var(--tw-ring-color);
            box-shadow: var(--tw-ring-offset-shadow),var(--tw-ring-shadow),var(--tw-shadow,0 0 transparent)
        }

        .form-check-input[type=radio] {
            width: 16px;
            height: 16px;
            position: relative;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            overflow: hidden;
            border-radius: 9999px;
            border-width: 1px;
            --tw-border-opacity: 1;
            border-color: rgba(226,232,240,var(--tw-border-opacity));
            padding: 0;
            --tw-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);
            box-shadow: var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),var(--tw-shadow)
        }

        .dark .form-check-input[type=radio] {
            --tw-border-opacity: 1;
            border-color: rgba(30,37,51,var(--tw-border-opacity));
            --tw-bg-opacity: 1;
            background-color: rgba(35,42,59,var(--tw-bg-opacity))
        }

        .form-check-input[type=radio]:before {
            content: "";
            width: 10px;
            height: 10px;
            transition: all .2s ease-in-out;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: auto;
            border-radius: 9999px;
            --tw-bg-opacity: 1;
            background-color: rgba(28,63,170,var(--tw-bg-opacity));
            opacity: 0
        }

        .form-check-input[type=radio]:checked {
            --tw-border-opacity: 1;
            border-color: rgba(28,63,170,var(--tw-border-opacity))
        }

        .form-check-input[type=radio]:checked:before {
            opacity: 1
        }

        .form-check-input[type=checkbox] {
            width: 16px;
            height: 16px;
            border-radius: .2em;
            position: relative;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            overflow: hidden;
            border-width: 1px;
            --tw-border-opacity: 1;
            border-color: rgba(226,232,240,var(--tw-border-opacity));
            padding: 0;
            --tw-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);
            box-shadow: var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),var(--tw-shadow)
        }

        .dark .form-check-input[type=checkbox] {
            --tw-border-opacity: 1;
            border-color: rgba(63,72,101,var(--tw-border-opacity));
            --tw-bg-opacity: 1;
            background-color: rgba(63,72,101,var(--tw-bg-opacity))
        }

        .form-check-input[type=checkbox]:before {
            content: "";
            transition: all .2s ease-in-out;
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='none' stroke='%23fff' stroke-width='3' stroke-linecap='round' stroke-linejoin='round' class='feather feather-check'%3E%3Cpath d='M20 6L9 17l-5-5'/%3E%3C/svg%3E");
            background-size: 80%;
            position: absolute;
            display: flex;
            height: 100%;
            width: 100%;
            align-items: center;
            justify-content: center;
            background-position: 50%;
            background-repeat: no-repeat;
            --tw-text-opacity: 1;
            color: rgba(255,255,255,var(--tw-text-opacity));
            opacity: 0
        }

        .dark .form-check-input[type=checkbox]:checked,.form-check-input[type=checkbox]:checked {
            --tw-border-opacity: 1;
            border-color: rgba(28,63,170,var(--tw-border-opacity));
            --tw-bg-opacity: 1;
            background-color: rgba(28,63,170,var(--tw-bg-opacity))
        }

        .form-check-input[type=checkbox]:checked:before {
            opacity: 1
        }

        .form-check-switch {
            width: 38px;
            height: 24px;
            padding: 1px;
            position: relative;
            margin-left: 0;
            margin-top: 0;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: 9999px;
            border-width: 1px;
            --tw-border-opacity: 1;
            border-color: rgba(226,232,240,var(--tw-border-opacity));
            background-image: none;
            --tw-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);
            box-shadow: var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),var(--tw-shadow);
            outline: 2px solid transparent;
            outline-offset: 2px
        }

        .dark .form-check-switch {
            --tw-border-opacity: 1;
            border-color: rgba(63,72,101,var(--tw-border-opacity));
            --tw-bg-opacity: 1;
            background-color: rgba(63,72,101,var(--tw-bg-opacity))
        }

        .form-check-switch:before {
            content: "";
            width: 22px;
            height: 22px;
            transition: all .2s ease-in-out;
            box-shadow: 1px 1px 3px rgba(0,0,0,.25);
            position: absolute;
            top: 0;
            bottom: 0;
            margin-top: auto;
            margin-bottom: auto;
            border-radius: 9999px
        }

        .dark .form-check-switch:before {
            --tw-bg-opacity: 1;
            background-color: rgba(41,49,69,var(--tw-bg-opacity))
        }

        .form-check-switch:checked {
            --tw-border-opacity: 1;
            border-color: rgba(28,63,170,var(--tw-border-opacity));
            --tw-bg-opacity: 1;
            background-color: rgba(28,63,170,var(--tw-bg-opacity))
        }

        .dark .form-check-switch:checked {
            --tw-bg-opacity: 1;
            background-color: rgba(49,96,216,var(--tw-bg-opacity))
        }

        .form-check-switch:checked:before {
            margin-left: 13px;
            --tw-bg-opacity: 1;
            background-color: rgba(255,255,255,var(--tw-bg-opacity))
        }

        .form-control {
            width: 100%;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: .375rem;
            border-width: 1px;
            padding: .5rem .75rem
        }

        .form-control::-moz-placeholder {
            --tw-placeholder-opacity: 1;
            color: rgba(160,174,192,var(--tw-placeholder-opacity))
        }

        .form-control:-ms-input-placeholder {
            --tw-placeholder-opacity: 1;
            color: rgba(160,174,192,var(--tw-placeholder-opacity))
        }

        .form-control::placeholder {
            --tw-placeholder-opacity: 1;
            color: rgba(160,174,192,var(--tw-placeholder-opacity))
        }

        .form-control {
            --tw-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);
            box-shadow: var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),var(--tw-shadow)
        }

        .dark .form-control {
            border-color: transparent;
            --tw-bg-opacity: 1;
            background-color: rgba(35,42,59,var(--tw-bg-opacity))
        }

        .form-control:focus {
            outline: 2px solid transparent;
            outline-offset: 2px;
            --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
            --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(3px + var(--tw-ring-offset-width)) var(--tw-ring-color);
            box-shadow: var(--tw-ring-offset-shadow),var(--tw-ring-shadow),var(--tw-shadow,0 0 transparent)
        }

        .form-control:disabled,.form-control[readonly] {
            cursor: not-allowed;
            --tw-bg-opacity: 1;
            background-color: rgba(247,250,252,var(--tw-bg-opacity))
        }

        .dark .form-control:disabled,.dark .form-control[readonly] {
            border-color: transparent;
            --tw-bg-opacity: 1;
            background-color: rgba(35,42,59,var(--tw-bg-opacity))
        }

        .form-control-sm {
            padding: .375rem .5rem;
            font-size: .75rem;
            line-height: 1rem
        }

        .form-control-lg {
            padding: .375rem 1rem;
            font-size: 1.125rem;
            line-height: 1.75rem
        }

        .form-control-rounded {
            border-radius: 9999px
        }

        .form-help {
            margin-top: .5rem;
            font-size: .75rem;
            line-height: 1rem;
            --tw-text-opacity: 1;
            color: rgba(113,128,150,var(--tw-text-opacity))
        }

        .form-inline {
            display: flex;
            align-items: center
        }

        @media (max-width: 639px) {
            .form-inline {
                display:block
            }
        }

        .form-inline .form-label {
            margin-bottom: 0;
            margin-right: 1.25rem;
            text-align: right
        }

        @media (max-width: 639px) {
            .form-inline .form-label {
                margin-bottom:.5rem;
                margin-right: 0;
                text-align: left
            }
        }

        .form-inline .form-control {
            flex: 1 1 0%
        }

        .form-label {
            margin-bottom: .5rem;
            display: inline-block
        }

        .form-select {
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='none' stroke='rgb(74 85 104)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-chevron-down'%3E%3Cpath d='M6 9l6 6 6-6'/%3E%3C/svg%3E");
            background-size: 18px;
            background-position: center right .6rem;
            width: 100%;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: .375rem;
            border-width: 1px;
            background-repeat: no-repeat;
            padding: .5rem 2rem .5rem .75rem
        }

        .form-select::-moz-placeholder {
            --tw-placeholder-opacity: 1;
            color: rgba(160,174,192,var(--tw-placeholder-opacity))
        }

        .form-select:-ms-input-placeholder {
            --tw-placeholder-opacity: 1;
            color: rgba(160,174,192,var(--tw-placeholder-opacity))
        }

        .form-select::placeholder {
            --tw-placeholder-opacity: 1;
            color: rgba(160,174,192,var(--tw-placeholder-opacity))
        }

        .form-select {
            --tw-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);
            box-shadow: var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),var(--tw-shadow)
        }

        .dark .form-select {
            border-color: transparent;
            --tw-bg-opacity: 1;
            background-color: rgba(35,42,59,var(--tw-bg-opacity))
        }

        .form-select:focus {
            outline: 2px solid transparent;
            outline-offset: 2px;
            --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
            --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(3px + var(--tw-ring-offset-width)) var(--tw-ring-color);
            box-shadow: var(--tw-ring-offset-shadow),var(--tw-ring-shadow),var(--tw-shadow,0 0 transparent)
        }

        .form-select:disabled,.form-select[readonly] {
            cursor: not-allowed;
            --tw-bg-opacity: 1;
            background-color: rgba(247,250,252,var(--tw-bg-opacity))
        }

        .dark .form-select,.form-select.form-select-light {
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='none' stroke='rgb(255 255 255)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-chevron-down'%3E%3Cpath d='M6 9l6 6 6-6'/%3E%3C/svg%3E")
        }

        .dark .form-select.form-select-dark {
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='none' stroke='rgb(74 85 104)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-chevron-down'%3E%3Cpath d='M6 9l6 6 6-6'/%3E%3C/svg%3E")
        }

        .form-select-sm {
            padding: .375rem 2rem .375rem .5rem;
            font-size: .75rem;
            line-height: 1rem
        }

        .form-select-lg {
            padding: .375rem 2rem .375rem 1rem;
            font-size: 1.125rem;
            line-height: 1.75rem
        }

        .image-fit {
            position: relative
        }

        .image-fit>img {
            position: absolute;
            height: 100%;
            width: 100%;
            -o-object-fit: cover;
            object-fit: cover
        }

        .input-group {
            display: flex
        }

        .input-group .input-group-text {
            border-width: 1px;
            --tw-border-opacity: 1;
            border-color: rgba(226,232,240,var(--tw-border-opacity));
            --tw-bg-opacity: 1;
            background-color: rgba(247,250,252,var(--tw-bg-opacity));
            padding: .5rem .75rem;
            --tw-text-opacity: 1;
            color: rgba(113,128,150,var(--tw-text-opacity));
            --tw-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);
            box-shadow: var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),var(--tw-shadow)
        }

        .dark .input-group .input-group-text {
            --tw-border-opacity: 1;
            border-color: rgba(30,37,51,var(--tw-border-opacity));
            --tw-bg-opacity: 1;
            background-color: rgba(41,49,69,var(--tw-bg-opacity))
        }

        .input-group>.form-control,.input-group>.input-group-text {
            border-radius: 0
        }

        .input-group>.form-control:not(:first-child),.input-group>.input-group-text:not(:first-child) {
            border-left-color: transparent
        }

        .input-group>.form-control:first-child,.input-group>.input-group-text:first-child {
            border-top-left-radius: .25rem;
            border-bottom-left-radius: .25rem
        }

        .input-group>.form-control:last-child,.input-group>.input-group-text:last-child {
            border-top-right-radius: .25rem;
            border-bottom-right-radius: .25rem
        }

        .input-group>.form-control {
            z-index: 10
        }
	</style>
	<form>
		<div class="mt-3 p-5 bg-white rounded-5 box">
			<label>Поиск:</label>
			<input type="search" name="name" class="border-2 p-1 rounded-md w-full"/>
		</div>
		<div class="mt-3 p-5 bg-white rounded-5 box">
			<label>Статус:</label>
			<div class="flex flex-col sm:flex-row mt-2">
				<?php foreach ($projectStateVariants as $index => $checkbox){ ?>
					<? 
						if(in_array($checkbox['ID'], [5,6,7])){
							$checked = 'checked';
						} else {
							$checked = '';
						}
					?>
					<div class="form-check mr-2 mt-2 sm:mt-0">
						<input id="checkbox-switch-<?=$index;?>" name="project_state" class="form-check-input" type="checkbox" value="<?=$checkbox['ID']?>" <?=$checked;?>>
						<label class="form-check-label" for="checkbox-switch-<?=$index;?>"><?=$checkbox['NAME']?></label>
					</div>
				<?php } ?>
			</div>
		</div>
	</form>
	<div id="content" class="intro-y intro-y grid grid-cols-12 gap-6 mt-5">
		<div class="loading" style="display: none;">
			<img src='/upload/loading.svg'>
		</div>
	</div>
	<div class="my-6 w-full text-center">
		<button type="button" class="btn mx-auto mb-2 loadmore" data-url="/local/ajax/project/" data-page="1"
		        style="background: rgb(71,158,154);color: white;font-size: 1.5rem;padding: 1.5rem;">
			<span>Показать ещё</span>
		</button>
	</div>

	<div id="card-template" style="display: none">
		<div class="card col-span-12 md:col-span-6 xl:col-span-4 box">
			<div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
				<div class="w-10 h-10 flex-none image-fit">
					<img alt="{{NAME}}" class="rounded-full" src="{{LOGO}}">
				</div>
				<div class="ml-3 mr-auto">
					<a href="" class="font-medium">{{NAME}}</a>
					<div class="flex text-gray-600 truncate text-xs mt-0.5">
						<a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="">{{STATE_NAME}}</a>
						<span class="mx-1">•</span> {{UPDATED}}
					</div>
				</div>
			</div>
			<div class="p-5">
				<div class="text-gray-700 dark:text-gray-600 mt-2">
					<p>{{DESCRIPTION}}</p>
					<p>Сайт: <a class="text-theme-1 dark:text-theme-10" href="{{SOURCE}}" target="_blank">{{SOURCE}}</a></p>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
        var USK = {
            loader: {
                selector: '.loading',
                show: function () {
                    $(this.selector).show();
                },
                hide: function () {
                    $(this.selector).hide();
                }
            },
            fill: function (element, template) {
                var placeholder = '';
                $.each(element, function (index, value) {
                    placeholder = '\{\{' + index + '\}\}';
                    template = template.replaceAll(placeholder, value);
                });
                return template;
            },
            project: {
                containerSelector: '#content',
                element: {
                    templateSelector: '#card-template',
                    getTemplate: function () {
                        let result = '';
                        let template = $(this.templateSelector);
                        if (template.length) {
                            result = template.html();
                        }
                        return result;
                    },
                    render: function (elementData) {
                        let template = this.getTemplate();
                        return USK.fill(elementData, template);
                    }
                },
                render: function (data) {
                    var container = $(this.containerSelector);
                    if (container.length) {
                        if (typeof (data) !== 'undefined') {
                            var elementHTML = '';
                            for (let i = 0; i < data.length; i++) {
                                elementHTML = USK.project.element.render(data[i]);
                                container.append(elementHTML);
                            }
                        }
                    }
                },
                clear : function (){
                	$('#content .card').remove();
                }
            }

        };

        jQuery('.loadmore').on('click', function () {
            USK.loader.show();
            var _this = $(this);
            var currentPage = parseInt(_this.data('page'));
            _this.prop('disabled', true);
            let url = _this.data('url');
            let project_state = [];
	        var n = $('[name="project_state"]:checked').length;
	        if (n > 0){
	            $('[name="project_state"]:checked').each(function(){
	                project_state.push($(this).val());
	            });
	        }
	        let name = $('[name="name"]').val();
            let params = {
                page: currentPage,
                project_state: project_state,
                name: name,
            };
            jQuery.ajax({
                url: url,
                type: "GET",
                data: params,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    setTimeout(function () {
                    	if(response.data.length){
                    		if(currentPage == 1){
    			        		USK.project.clear();
                    		}
                        	USK.project.render(response.data);
	                        _this.data('page', ++currentPage);
	                        _this.prop('disabled', false);
                    	} 
                    	if(response.limit > response.count){
                    		_this.prop('disabled', true);
                    	}
                    	if(response.data.length === 0){
                    		USK.project.clear();
                    	}
                        USK.loader.hide();
                    }, 1000);
                }
            });
        });

        $('[name="project_state"]').on('change', function(){
        	jQuery('.loadmore').data('page', 1);
        	jQuery('.loadmore').prop('disabled', false);
        	jQuery('.loadmore').click();
        });

        $('[name="name"]').on('blur', function(){
        	jQuery('.loadmore').data('page', 1);
        	jQuery('.loadmore').prop('disabled', false);
        	jQuery('.loadmore').click();
        });

        $('[name="name"]').on('keypress',function(e) {
		    if(e.which == 13) {
		        jQuery('.loadmore').data('page', 1);
	        	jQuery('.loadmore').prop('disabled', false);
	        	jQuery('.loadmore').click();
		    	return false;
		    }
		});


        jQuery(document).ready(function () {
            jQuery('.loadmore').click();
        });
	</script>

<?
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog.php"); ?>