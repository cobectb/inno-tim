<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog.php"); ?>

	<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
		<h2 class="text-lg font-medium mr-auto">
			Blog Layout
		</h2>
		<div class="w-full sm:w-auto flex mt-4 sm:mt-0">
			<button class="btn btn-primary shadow-md mr-2">Add New Post</button>
			<div class="dropdown ml-auto sm:ml-0">
				<button class="dropdown-toggle btn px-2 box text-gray-700 dark:text-gray-300" aria-expanded="false">
					<span class="w-5 h-5 flex items-center justify-center"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus w-4 h-4"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg> </span>
				</button>
				<div class="dropdown-menu w-40">
					<div class="dropdown-menu__content box dark:bg-dark-1 p-2">
						<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2 w-4 h-4 mr-2"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg> Share Post </a>
						<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-download w-4 h-4 mr-2"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="7 10 12 15 17 10"></polyline><line x1="12" y1="15" x2="12" y2="3"></line></svg> Download Post </a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="intro-y grid grid-cols-12 gap-6 mt-5">
		<!-- BEGIN: Blog Layout -->
		<div class="intro-y col-span-12 md:col-span-6 xl:col-span-4 box">
			<div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
				<div class="w-10 h-10 flex-none image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-1.jpg">
				</div>
				<div class="ml-3 mr-auto">
					<a href="" class="font-medium">John Travolta</a>
					<div class="flex text-gray-600 truncate text-xs mt-0.5"> <a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="">Sport &amp; Outdoor</a> <span class="mx-1">•</span> 55 minutes ago </div>
				</div>
				<div class="dropdown ml-3">
					<a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-600 dark:text-gray-300" aria-expanded="false"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical w-4 h-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg> </a>
					<div class="dropdown-menu w-40">
						<div class="dropdown-menu__content box dark:bg-dark-1 p-2">
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 w-4 h-4 mr-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg> Edit Post </a>
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4 mr-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> Delete Post </a>
						</div>
					</div>
				</div>
			</div>
			<div class="p-5">
				<div class="h-40 xxl:h-56 image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="dist/images/preview-7.jpg">
				</div>
				<a href="" class="block font-medium text-base mt-5">Dummy text of the printing and typesetting industry</a>
				<div class="text-gray-700 dark:text-gray-600 mt-2">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem </div>
			</div>
			<div class="flex items-center px-5 py-3 border-t border-gray-200 dark:border-dark-5">
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full border border-gray-400 dark:border-dark-5 dark:bg-dark-5 dark:text-gray-300 text-gray-600 mr-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark w-3 h-3"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg> </a>
				<div class="intro-x flex mr-2">
					<div class="intro-x w-8 h-8 image-fit">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-1.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-1.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-9.jpg">
					</div>
				</div>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-14 dark:bg-dark-5 dark:text-gray-300 text-theme-10 ml-auto tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2 w-3 h-3"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg> </a>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-1 text-white ml-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share w-3 h-3"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg> </a>
			</div>
			<div class="px-5 pt-3 pb-5 border-t border-gray-200 dark:border-dark-5">
				<div class="w-full flex text-gray-600 text-xs sm:text-sm">
					<div class="mr-2"> Comments: <span class="font-medium">29</span> </div>
					<div class="mr-2"> Views: <span class="font-medium">109k</span> </div>
					<div class="ml-auto"> Likes: <span class="font-medium">43k</span> </div>
				</div>
				<div class="w-full flex items-center mt-3">
					<div class="w-8 h-8 flex-none image-fit mr-3">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-1.jpg">
					</div>
					<div class="flex-1 relative text-gray-700">
						<input type="text" class="form-control form-control-rounded border-transparent bg-gray-200 pr-10 placeholder-theme-13" placeholder="Post a comment...">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
					</div>
				</div>
			</div>
		</div>
		<div class="intro-y col-span-12 md:col-span-6 xl:col-span-4 box">
			<div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
				<div class="w-10 h-10 flex-none image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-7.jpg">
				</div>
				<div class="ml-3 mr-auto">
					<a href="" class="font-medium">Robert De Niro</a>
					<div class="flex text-gray-600 truncate text-xs mt-0.5"> <a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="">Electronic</a> <span class="mx-1">•</span> 44 minutes ago </div>
				</div>
				<div class="dropdown ml-3">
					<a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-600 dark:text-gray-300" aria-expanded="false"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical w-4 h-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg> </a>
					<div class="dropdown-menu w-40">
						<div class="dropdown-menu__content box dark:bg-dark-1 p-2">
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 w-4 h-4 mr-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg> Edit Post </a>
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4 mr-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> Delete Post </a>
						</div>
					</div>
				</div>
			</div>
			<div class="p-5">
				<div class="h-40 xxl:h-56 image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="dist/images/preview-9.jpg">
				</div>
				<a href="" class="block font-medium text-base mt-5">Dummy text of the printing and typesetting industry</a>
				<div class="text-gray-700 dark:text-gray-600 mt-2">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem </div>
			</div>
			<div class="flex items-center px-5 py-3 border-t border-gray-200 dark:border-dark-5">
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full border border-gray-400 dark:border-dark-5 dark:bg-dark-5 dark:text-gray-300 text-gray-600 mr-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark w-3 h-3"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg> </a>
				<div class="intro-x flex mr-2">
					<div class="intro-x w-8 h-8 image-fit">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-7.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-11.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-9.jpg">
					</div>
				</div>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-14 dark:bg-dark-5 dark:text-gray-300 text-theme-10 ml-auto tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2 w-3 h-3"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg> </a>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-1 text-white ml-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share w-3 h-3"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg> </a>
			</div>
			<div class="px-5 pt-3 pb-5 border-t border-gray-200 dark:border-dark-5">
				<div class="w-full flex text-gray-600 text-xs sm:text-sm">
					<div class="mr-2"> Comments: <span class="font-medium">22</span> </div>
					<div class="mr-2"> Views: <span class="font-medium">24k</span> </div>
					<div class="ml-auto"> Likes: <span class="font-medium">98k</span> </div>
				</div>
				<div class="w-full flex items-center mt-3">
					<div class="w-8 h-8 flex-none image-fit mr-3">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-7.jpg">
					</div>
					<div class="flex-1 relative text-gray-700">
						<input type="text" class="form-control form-control-rounded border-transparent bg-gray-200 pr-10 placeholder-theme-13" placeholder="Post a comment...">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
					</div>
				</div>
			</div>
		</div>
		<div class="intro-y col-span-12 md:col-span-6 xl:col-span-4 box">
			<div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
				<div class="w-10 h-10 flex-none image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-5.jpg">
				</div>
				<div class="ml-3 mr-auto">
					<a href="" class="font-medium">Angelina Jolie</a>
					<div class="flex text-gray-600 truncate text-xs mt-0.5"> <a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="">Electronic</a> <span class="mx-1">•</span> 40 minutes ago </div>
				</div>
				<div class="dropdown ml-3">
					<a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-600 dark:text-gray-300" aria-expanded="false"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical w-4 h-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg> </a>
					<div class="dropdown-menu w-40">
						<div class="dropdown-menu__content box dark:bg-dark-1 p-2">
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 w-4 h-4 mr-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg> Edit Post </a>
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4 mr-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> Delete Post </a>
						</div>
					</div>
				</div>
			</div>
			<div class="p-5">
				<div class="h-40 xxl:h-56 image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="dist/images/preview-6.jpg">
				</div>
				<a href="" class="block font-medium text-base mt-5">200 Latin words, combined with a handful of model sentences</a>
				<div class="text-gray-700 dark:text-gray-600 mt-2">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomi</div>
			</div>
			<div class="flex items-center px-5 py-3 border-t border-gray-200 dark:border-dark-5">
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full border border-gray-400 dark:border-dark-5 dark:bg-dark-5 dark:text-gray-300 text-gray-600 mr-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark w-3 h-3"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg> </a>
				<div class="intro-x flex mr-2">
					<div class="intro-x w-8 h-8 image-fit">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-5.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-9.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-1.jpg">
					</div>
				</div>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-14 dark:bg-dark-5 dark:text-gray-300 text-theme-10 ml-auto tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2 w-3 h-3"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg> </a>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-1 text-white ml-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share w-3 h-3"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg> </a>
			</div>
			<div class="px-5 pt-3 pb-5 border-t border-gray-200 dark:border-dark-5">
				<div class="w-full flex text-gray-600 text-xs sm:text-sm">
					<div class="mr-2"> Comments: <span class="font-medium">29</span> </div>
					<div class="mr-2"> Views: <span class="font-medium">57k</span> </div>
					<div class="ml-auto"> Likes: <span class="font-medium">23k</span> </div>
				</div>
				<div class="w-full flex items-center mt-3">
					<div class="w-8 h-8 flex-none image-fit mr-3">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-5.jpg">
					</div>
					<div class="flex-1 relative text-gray-700">
						<input type="text" class="form-control form-control-rounded border-transparent bg-gray-200 pr-10 placeholder-theme-13" placeholder="Post a comment...">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
					</div>
				</div>
			</div>
		</div>
		<div class="intro-y col-span-12 md:col-span-6 xl:col-span-4 box">
			<div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
				<div class="w-10 h-10 flex-none image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-9.jpg">
				</div>
				<div class="ml-3 mr-auto">
					<a href="" class="font-medium">Bruce Willis</a>
					<div class="flex text-gray-600 truncate text-xs mt-0.5"> <a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="">PC &amp; Laptop</a> <span class="mx-1">•</span> 54 seconds ago </div>
				</div>
				<div class="dropdown ml-3">
					<a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-600 dark:text-gray-300" aria-expanded="false"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical w-4 h-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg> </a>
					<div class="dropdown-menu w-40">
						<div class="dropdown-menu__content box dark:bg-dark-1 p-2">
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 w-4 h-4 mr-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg> Edit Post </a>
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4 mr-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> Delete Post </a>
						</div>
					</div>
				</div>
			</div>
			<div class="p-5">
				<div class="h-40 xxl:h-56 image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="dist/images/preview-8.jpg">
				</div>
				<a href="" class="block font-medium text-base mt-5">200 Latin words, combined with a handful of model sentences</a>
				<div class="text-gray-700 dark:text-gray-600 mt-2">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomi</div>
			</div>
			<div class="flex items-center px-5 py-3 border-t border-gray-200 dark:border-dark-5">
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full border border-gray-400 dark:border-dark-5 dark:bg-dark-5 dark:text-gray-300 text-gray-600 mr-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark w-3 h-3"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg> </a>
				<div class="intro-x flex mr-2">
					<div class="intro-x w-8 h-8 image-fit">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-9.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-7.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-15.jpg">
					</div>
				</div>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-14 dark:bg-dark-5 dark:text-gray-300 text-theme-10 ml-auto tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2 w-3 h-3"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg> </a>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-1 text-white ml-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share w-3 h-3"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg> </a>
			</div>
			<div class="px-5 pt-3 pb-5 border-t border-gray-200 dark:border-dark-5">
				<div class="w-full flex text-gray-600 text-xs sm:text-sm">
					<div class="mr-2"> Comments: <span class="font-medium">87</span> </div>
					<div class="mr-2"> Views: <span class="font-medium">47k</span> </div>
					<div class="ml-auto"> Likes: <span class="font-medium">168k</span> </div>
				</div>
				<div class="w-full flex items-center mt-3">
					<div class="w-8 h-8 flex-none image-fit mr-3">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-9.jpg">
					</div>
					<div class="flex-1 relative text-gray-700">
						<input type="text" class="form-control form-control-rounded border-transparent bg-gray-200 pr-10 placeholder-theme-13" placeholder="Post a comment...">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
					</div>
				</div>
			</div>
		</div>
		<div class="intro-y col-span-12 md:col-span-6 xl:col-span-4 box">
			<div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
				<div class="w-10 h-10 flex-none image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-3.jpg">
				</div>
				<div class="ml-3 mr-auto">
					<a href="" class="font-medium">Russell Crowe</a>
					<div class="flex text-gray-600 truncate text-xs mt-0.5"> <a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="">Electronic</a> <span class="mx-1">•</span> 14 seconds ago </div>
				</div>
				<div class="dropdown ml-3">
					<a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-600 dark:text-gray-300" aria-expanded="false"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical w-4 h-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg> </a>
					<div class="dropdown-menu w-40">
						<div class="dropdown-menu__content box dark:bg-dark-1 p-2">
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 w-4 h-4 mr-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg> Edit Post </a>
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4 mr-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> Delete Post </a>
						</div>
					</div>
				</div>
			</div>
			<div class="p-5">
				<div class="h-40 xxl:h-56 image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="dist/images/preview-14.jpg">
				</div>
				<a href="" class="block font-medium text-base mt-5">Popularised in the 1960s with the release of Letraset</a>
				<div class="text-gray-700 dark:text-gray-600 mt-2">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 20</div>
			</div>
			<div class="flex items-center px-5 py-3 border-t border-gray-200 dark:border-dark-5">
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full border border-gray-400 dark:border-dark-5 dark:bg-dark-5 dark:text-gray-300 text-gray-600 mr-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark w-3 h-3"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg> </a>
				<div class="intro-x flex mr-2">
					<div class="intro-x w-8 h-8 image-fit">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-3.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-2.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-14.jpg">
					</div>
				</div>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-14 dark:bg-dark-5 dark:text-gray-300 text-theme-10 ml-auto tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2 w-3 h-3"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg> </a>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-1 text-white ml-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share w-3 h-3"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg> </a>
			</div>
			<div class="px-5 pt-3 pb-5 border-t border-gray-200 dark:border-dark-5">
				<div class="w-full flex text-gray-600 text-xs sm:text-sm">
					<div class="mr-2"> Comments: <span class="font-medium">20</span> </div>
					<div class="mr-2"> Views: <span class="font-medium">63k</span> </div>
					<div class="ml-auto"> Likes: <span class="font-medium">115k</span> </div>
				</div>
				<div class="w-full flex items-center mt-3">
					<div class="w-8 h-8 flex-none image-fit mr-3">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-3.jpg">
					</div>
					<div class="flex-1 relative text-gray-700">
						<input type="text" class="form-control form-control-rounded border-transparent bg-gray-200 pr-10 placeholder-theme-13" placeholder="Post a comment...">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
					</div>
				</div>
			</div>
		</div>
		<div class="intro-y col-span-12 md:col-span-6 xl:col-span-4 box">
			<div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
				<div class="w-10 h-10 flex-none image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-9.jpg">
				</div>
				<div class="ml-3 mr-auto">
					<a href="" class="font-medium">Brad Pitt</a>
					<div class="flex text-gray-600 truncate text-xs mt-0.5"> <a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="">Electronic</a> <span class="mx-1">•</span> 57 seconds ago </div>
				</div>
				<div class="dropdown ml-3">
					<a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-600 dark:text-gray-300" aria-expanded="false"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical w-4 h-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg> </a>
					<div class="dropdown-menu w-40">
						<div class="dropdown-menu__content box dark:bg-dark-1 p-2">
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 w-4 h-4 mr-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg> Edit Post </a>
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4 mr-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> Delete Post </a>
						</div>
					</div>
				</div>
			</div>
			<div class="p-5">
				<div class="h-40 xxl:h-56 image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="dist/images/preview-11.jpg">
				</div>
				<a href="" class="block font-medium text-base mt-5">Popularised in the 1960s with the release of Letraset</a>
				<div class="text-gray-700 dark:text-gray-600 mt-2">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 20</div>
			</div>
			<div class="flex items-center px-5 py-3 border-t border-gray-200 dark:border-dark-5">
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full border border-gray-400 dark:border-dark-5 dark:bg-dark-5 dark:text-gray-300 text-gray-600 mr-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark w-3 h-3"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg> </a>
				<div class="intro-x flex mr-2">
					<div class="intro-x w-8 h-8 image-fit">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-9.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-15.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-2.jpg">
					</div>
				</div>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-14 dark:bg-dark-5 dark:text-gray-300 text-theme-10 ml-auto tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2 w-3 h-3"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg> </a>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-1 text-white ml-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share w-3 h-3"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg> </a>
			</div>
			<div class="px-5 pt-3 pb-5 border-t border-gray-200 dark:border-dark-5">
				<div class="w-full flex text-gray-600 text-xs sm:text-sm">
					<div class="mr-2"> Comments: <span class="font-medium">93</span> </div>
					<div class="mr-2"> Views: <span class="font-medium">29k</span> </div>
					<div class="ml-auto"> Likes: <span class="font-medium">66k</span> </div>
				</div>
				<div class="w-full flex items-center mt-3">
					<div class="w-8 h-8 flex-none image-fit mr-3">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-9.jpg">
					</div>
					<div class="flex-1 relative text-gray-700">
						<input type="text" class="form-control form-control-rounded border-transparent bg-gray-200 pr-10 placeholder-theme-13" placeholder="Post a comment...">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
					</div>
				</div>
			</div>
		</div>
		<div class="intro-y col-span-12 md:col-span-6 xl:col-span-4 box">
			<div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
				<div class="w-10 h-10 flex-none image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-3.jpg">
				</div>
				<div class="ml-3 mr-auto">
					<a href="" class="font-medium">John Travolta</a>
					<div class="flex text-gray-600 truncate text-xs mt-0.5"> <a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="">Electronic</a> <span class="mx-1">•</span> 33 seconds ago </div>
				</div>
				<div class="dropdown ml-3">
					<a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-600 dark:text-gray-300" aria-expanded="false"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical w-4 h-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg> </a>
					<div class="dropdown-menu w-40">
						<div class="dropdown-menu__content box dark:bg-dark-1 p-2">
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 w-4 h-4 mr-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg> Edit Post </a>
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4 mr-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> Delete Post </a>
						</div>
					</div>
				</div>
			</div>
			<div class="p-5">
				<div class="h-40 xxl:h-56 image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="dist/images/preview-8.jpg">
				</div>
				<a href="" class="block font-medium text-base mt-5">200 Latin words, combined with a handful of model sentences</a>
				<div class="text-gray-700 dark:text-gray-600 mt-2">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomi</div>
			</div>
			<div class="flex items-center px-5 py-3 border-t border-gray-200 dark:border-dark-5">
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full border border-gray-400 dark:border-dark-5 dark:bg-dark-5 dark:text-gray-300 text-gray-600 mr-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark w-3 h-3"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg> </a>
				<div class="intro-x flex mr-2">
					<div class="intro-x w-8 h-8 image-fit">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-3.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-5.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-5.jpg">
					</div>
				</div>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-14 dark:bg-dark-5 dark:text-gray-300 text-theme-10 ml-auto tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2 w-3 h-3"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg> </a>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-1 text-white ml-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share w-3 h-3"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg> </a>
			</div>
			<div class="px-5 pt-3 pb-5 border-t border-gray-200 dark:border-dark-5">
				<div class="w-full flex text-gray-600 text-xs sm:text-sm">
					<div class="mr-2"> Comments: <span class="font-medium">50</span> </div>
					<div class="mr-2"> Views: <span class="font-medium">43k</span> </div>
					<div class="ml-auto"> Likes: <span class="font-medium">84k</span> </div>
				</div>
				<div class="w-full flex items-center mt-3">
					<div class="w-8 h-8 flex-none image-fit mr-3">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-3.jpg">
					</div>
					<div class="flex-1 relative text-gray-700">
						<input type="text" class="form-control form-control-rounded border-transparent bg-gray-200 pr-10 placeholder-theme-13" placeholder="Post a comment...">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
					</div>
				</div>
			</div>
		</div>
		<div class="intro-y col-span-12 md:col-span-6 xl:col-span-4 box">
			<div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
				<div class="w-10 h-10 flex-none image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-15.jpg">
				</div>
				<div class="ml-3 mr-auto">
					<a href="" class="font-medium">Nicolas Cage</a>
					<div class="flex text-gray-600 truncate text-xs mt-0.5"> <a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="">Smartphone &amp; Tablet</a> <span class="mx-1">•</span> 37 seconds ago </div>
				</div>
				<div class="dropdown ml-3">
					<a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-600 dark:text-gray-300" aria-expanded="false"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical w-4 h-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg> </a>
					<div class="dropdown-menu w-40">
						<div class="dropdown-menu__content box dark:bg-dark-1 p-2">
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 w-4 h-4 mr-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg> Edit Post </a>
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4 mr-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> Delete Post </a>
						</div>
					</div>
				</div>
			</div>
			<div class="p-5">
				<div class="h-40 xxl:h-56 image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="dist/images/preview-7.jpg">
				</div>
				<a href="" class="block font-medium text-base mt-5">Desktop publishing software like Aldus PageMaker</a>
				<div class="text-gray-700 dark:text-gray-600 mt-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500</div>
			</div>
			<div class="flex items-center px-5 py-3 border-t border-gray-200 dark:border-dark-5">
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full border border-gray-400 dark:border-dark-5 dark:bg-dark-5 dark:text-gray-300 text-gray-600 mr-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark w-3 h-3"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg> </a>
				<div class="intro-x flex mr-2">
					<div class="intro-x w-8 h-8 image-fit">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-15.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-3.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-2.jpg">
					</div>
				</div>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-14 dark:bg-dark-5 dark:text-gray-300 text-theme-10 ml-auto tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2 w-3 h-3"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg> </a>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-1 text-white ml-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share w-3 h-3"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg> </a>
			</div>
			<div class="px-5 pt-3 pb-5 border-t border-gray-200 dark:border-dark-5">
				<div class="w-full flex text-gray-600 text-xs sm:text-sm">
					<div class="mr-2"> Comments: <span class="font-medium">25</span> </div>
					<div class="mr-2"> Views: <span class="font-medium">76k</span> </div>
					<div class="ml-auto"> Likes: <span class="font-medium">204k</span> </div>
				</div>
				<div class="w-full flex items-center mt-3">
					<div class="w-8 h-8 flex-none image-fit mr-3">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-15.jpg">
					</div>
					<div class="flex-1 relative text-gray-700">
						<input type="text" class="form-control form-control-rounded border-transparent bg-gray-200 pr-10 placeholder-theme-13" placeholder="Post a comment...">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
					</div>
				</div>
			</div>
		</div>
		<div class="intro-y col-span-12 md:col-span-6 xl:col-span-4 box">
			<div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
				<div class="w-10 h-10 flex-none image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-1.jpg">
				</div>
				<div class="ml-3 mr-auto">
					<a href="" class="font-medium">Russell Crowe</a>
					<div class="flex text-gray-600 truncate text-xs mt-0.5"> <a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="">Smartphone &amp; Tablet</a> <span class="mx-1">•</span> 58 seconds ago </div>
				</div>
				<div class="dropdown ml-3">
					<a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-600 dark:text-gray-300" aria-expanded="false"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical w-4 h-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg> </a>
					<div class="dropdown-menu w-40">
						<div class="dropdown-menu__content box dark:bg-dark-1 p-2">
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 w-4 h-4 mr-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg> Edit Post </a>
							<a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4 mr-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> Delete Post </a>
						</div>
					</div>
				</div>
			</div>
			<div class="p-5">
				<div class="h-40 xxl:h-56 image-fit">
					<img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="dist/images/preview-5.jpg">
				</div>
				<a href="" class="block font-medium text-base mt-5">Popularised in the 1960s with the release of Letraset</a>
				<div class="text-gray-700 dark:text-gray-600 mt-2">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 20</div>
			</div>
			<div class="flex items-center px-5 py-3 border-t border-gray-200 dark:border-dark-5">
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full border border-gray-400 dark:border-dark-5 dark:bg-dark-5 dark:text-gray-300 text-gray-600 mr-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark w-3 h-3"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg> </a>
				<div class="intro-x flex mr-2">
					<div class="intro-x w-8 h-8 image-fit">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-1.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-14.jpg">
					</div>
					<div class="intro-x w-8 h-8 image-fit -ml-4">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-9.jpg">
					</div>
				</div>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-14 dark:bg-dark-5 dark:text-gray-300 text-theme-10 ml-auto tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2 w-3 h-3"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg> </a>
				<a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-1 text-white ml-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share w-3 h-3"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg> </a>
			</div>
			<div class="px-5 pt-3 pb-5 border-t border-gray-200 dark:border-dark-5">
				<div class="w-full flex text-gray-600 text-xs sm:text-sm">
					<div class="mr-2"> Comments: <span class="font-medium">34</span> </div>
					<div class="mr-2"> Views: <span class="font-medium">122k</span> </div>
					<div class="ml-auto"> Likes: <span class="font-medium">27k</span> </div>
				</div>
				<div class="w-full flex items-center mt-3">
					<div class="w-8 h-8 flex-none image-fit mr-3">
						<img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-1.jpg">
					</div>
					<div class="flex-1 relative text-gray-700">
						<input type="text" class="form-control form-control-rounded border-transparent bg-gray-200 pr-10 placeholder-theme-13" placeholder="Post a comment...">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
					</div>
				</div>
			</div>
		</div>
		<!-- END: Blog Layout -->
		<!-- BEGIN: Pagination -->
		<div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
			<ul class="pagination">
				<li>
					<a class="pagination__link" href=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevrons-left w-4 h-4"><polyline points="11 17 6 12 11 7"></polyline><polyline points="18 17 13 12 18 7"></polyline></svg> </a>
				</li>
				<li>
					<a class="pagination__link" href=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left w-4 h-4"><polyline points="15 18 9 12 15 6"></polyline></svg> </a>
				</li>
				<li> <a class="pagination__link" href="">...</a> </li>
				<li> <a class="pagination__link" href="">1</a> </li>
				<li> <a class="pagination__link pagination__link--active" href="">2</a> </li>
				<li> <a class="pagination__link" href="">3</a> </li>
				<li> <a class="pagination__link" href="">...</a> </li>
				<li>
					<a class="pagination__link" href=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right w-4 h-4"><polyline points="9 18 15 12 9 6"></polyline></svg> </a>
				</li>
				<li>
					<a class="pagination__link" href=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevrons-right w-4 h-4"><polyline points="13 17 18 12 13 7"></polyline><polyline points="6 17 11 12 6 7"></polyline></svg> </a>
				</li>
			</ul>
			<select class="w-20 form-select box mt-3 sm:mt-0">
				<option>10</option>
				<option>25</option>
				<option>35</option>
				<option>50</option>
			</select>
		</div>
		<!-- END: Pagination -->
	</div>


<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog.php");?>